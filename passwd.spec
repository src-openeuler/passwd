Name:       passwd
Version:    0.80
Release:    11
Summary:    An implementation of the password setting/changing utility through PAM and libuser libraries
License:    BSD or GPL+
URL:        https://pagure.io/passwd
Source0:    https://releases.pagure.org/passwd/passwd-%{version}.autotoolized.tar.bz2

Patch1: fix-incorrect-S-output.patch
Patch9000: add-sm3-crypt-support.patch
Patch9001: zh_CN_po_modify.patch

BuildRequires:libselinux-devel >= 2.1.6-3 gcc glib2-devel, libuser-devel, pam-devel, libuser >= 0.53-1
BuildRequires: gettext, popt-devel audit-libs-devel >= 2.4.5 
Requires:libselinux >= 2.1.6-3 audit-libs >= 2.4.5 pam >= 1.0.90, /etc/pam.d/system-auth

%description
The package contains an implementation of the password
setting/changing utility through PAM(Pluggable Authentication
Modules) and libuser libraries.

%package_help

%prep
%autosetup -n %{name}-%{version} -p1

%build
%configure --with-selinux  --with-audit
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
%make_install
install -m 755 -d %{buildroot}%{_sysconfdir}/pam.d/
install -m 644 passwd.pamd %{buildroot}%{_sysconfdir}/pam.d/passwd
%find_lang %{name}

%check
make check

%pre

%preun

%post

%postun

%files -f %{name}.lang
%defattr(-,root,root)
%license COPYING AUTHORS
%doc ChangeLog NEWS
%attr(4755,root,root) %{_bindir}/*
%config(noreplace) %{_sysconfdir}/pam.d/passwd

%files help
%defattr(-,root,root)
%{_mandir}/ja/man1/*.gz
%{_mandir}/man1/*.gz

%changelog
* Fri Jul 19 2024 liyue01 <liyue01@kylinos.cn> - 0.80-11
- modify translation in the zh_CN.po

* Mon Aug 15 2022 xueyamao <xueyamao@kylinos.cn> - 0.80-10
- Type:defect
- ID:NA
- SUG:NA
- DESC:fix incorrect -S output

* Thu Oct 28 2021 lujie <lujie42@huawei.com> - 0.80-9
- Type:requirement
- ID:NA
- SUG:NA
- DESC:add sm3 crypt support

* Fri Sep 10 2021 Hugel <gengqihu1@huawei.com> - 0.80-8
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:depend on audit-libs instead of audit

* Wed Oct 9 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.80-7
- Type:enhancement
- ID:NA
- SUG:NA
- DESC: change the directory of AUTHORS

* Tue Sep 24 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.80-6
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:revise requires

* Wed Aug 21 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.80-5
- Package init
